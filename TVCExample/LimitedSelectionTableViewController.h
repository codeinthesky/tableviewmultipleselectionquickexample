//
//  LimitedSelectionTableViewController.h
//  TVCExample
//
//  Created by Nick Hain on 7/16/15.
//  Copyright (c) 2015 Empty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LimitedSelectionTableViewController : UITableViewController

-(NSArray *)selectedPlaceNames;

@end
