//
//  TableViewCell.m
//  TVCExample
//
//  Created by Nick Hain on 7/16/15.
//  Copyright (c) 2015 Empty. All rights reserved.
//

#import "TableViewCell.h"

@interface TableViewCell()

@property (weak, nonatomic) IBOutlet UITextField *myTextField;
@property (weak, nonatomic) IBOutlet UIImageView *myImageView;

@end

@implementation TableViewCell

- (void)awakeFromNib {
    // set check mark off when instantiated.
   self.imageView.hidden = YES;
}

-(void)configureWithDisplayText:(NSString *)text{
   self.myTextField.text = text;
}

// overide
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
   [super setSelected:selected animated:animated];
   
   // don't show gray background upon selection
   self.selectionStyle = UITableViewCellSelectionStyleNone;
   
   // do show check mark image when selected
   self.myImageView.hidden = !selected;
   
   // Configure the view for the selected state
}

@end
