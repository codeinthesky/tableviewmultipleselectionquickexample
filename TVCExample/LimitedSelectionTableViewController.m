//
//  LimitedSelectionTableViewController.m
//  TVCExample
//
//  Created by Nick Hain on 7/16/15.
//  Copyright (c) 2015 Empty. All rights reserved.
//

#import "LimitedSelectionTableViewController.h"
#import "TableViewCell.h"

@interface LimitedSelectionTableViewController ()

@property (nonatomic, strong) NSArray *placeNames;

@end

@implementation LimitedSelectionTableViewController

-(NSArray *)selectedPlaceNames{
   NSMutableArray *selectedNames = [[NSMutableArray alloc] init];
   for (NSIndexPath *indexPath in [self.tableView indexPathsForSelectedRows]) {
      NSString *selectedName = [self.placeNames objectAtIndex:indexPath.row];
      [selectedNames addObject:selectedName];
   }
   return selectedNames;
}

-(NSArray *)placeNames{
   if (!_placeNames) {
      _placeNames = @[@"San Francisco",
                      @"San Jose",
                      @"LA",
                      @"Chicago",
                      @"DisneyLand",
                      @"Key West",
                      @"Detroit",
                      @"Springfield",
                      @"Palo Alto"];
   }
   return _placeNames;
}

#define kCellReuseIdentifier @"kMyCellReuseIdentifier"
- (void)viewDidLoad {
    [super viewDidLoad];
   
   self.tableView.allowsMultipleSelection = YES;
   
   [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TableViewCell class])
                                              bundle:[NSBundle bundleForClass:[TableViewCell class]]]
        forCellReuseIdentifier:kCellReuseIdentifier];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate


#define kMaximumNumberOfSelections 3

-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   if ([[tableView indexPathsForSelectedRows] count] >= kMaximumNumberOfSelections) {
      
      // show alert informing user they can't select more than maximimum number of cells
      UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No more than three"
                                                     message:@"You can't select more than three cells"
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles: nil];
      
      [alert show];
      // prevent selection of row by returning nil.
      return nil;
   } else {
      // carry on like normal. nothing to see here.
      return indexPath;
   }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.placeNames count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifier forIndexPath:indexPath];
   
    // Configure the cell...
   [cell configureWithDisplayText:[self.placeNames objectAtIndex:indexPath.row]];
    
    return cell;
}




@end
